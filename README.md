# Code organization

**REPOSITORY** -> 1 or more MODULES  
     A Go repository typically contains only one module, located at the root of the repository. A file named go.mod there declares the module path

**MODULE** -> collection of PACKAGES  

**PACKAGE** -> sources compiled together  

## Full structure templates
* Template structure - https://github.com/golang-standards/project-layout
* One of examples with "good quality" marks - https://github.com/gopasspw/gopass

## Examples of project structure variety

### Workspace level - under $GOPATH
![workspace level](general_workspace_structure.jpg)


### Project level
##### example 1
![workspace level](project_struct_1.png)

#### example 2 
![workspace level](project_struct_2.jpg)


#### example 3 with tests
![workspace level](project_struct_tests_3.jpg)

 
## Module file example

```
module golang.org/examples/helloworld
go 1.13
require (
	github.com/gorilla/context v1.1.1
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	golang.org/x/text v0.3.2
	....
)
```
```
> go mod init company.com/my/repo
```
* https://github.com/golang/go/wiki/Modules
* https://blog.golang.org/migrating-to-go-modules


## How to write code
https://golang.org/doc/code.html


# Code style approaches
https://golang.org/doc/effective_go.html#commentary

Practical Go: Real world advice for writing maintainable Go program:
* https://dave.cheney.net/practical-go/presentations/qcon-china.html

Code styles
* https://dmitri.shuralyov.com/idiomatic-go

## Names
Names are as important in Go as in any other language. 
They even have semantic effect: the visibility of a name outside a package 
is determined by whether its first character is upper case.

# Test approaches

```
go test
```
Naming:
```
func TestXxx(*testing.T)
```

**Conclusion**: Not so clear for BDD style: 
```
func TestShouldCreateAccount(*testing.T)
```

## Testing package
https://golang.org/pkg/testing/

## BDD
#### With Godog - Cucumber for Golang
```
import (
	"github.com/DATA-DOG/godog"
    ...
}
func FeatureContext(s *godog.Suite) {
    ...
    s.Step(`^initialized service$`, createService)
	s.Step(`^the status code is "([^"]*)"\$$`, statusCodeIs)
    ..
}
	
func createService() (err error) {
	theService, err = service.Create(nil)
	return
}
..
```

```
Feature: Account
  Service must provide a way to create account 

  Scenario Template: create an account
    Given initialized service
    When I create an account
    Then the status code is <expected_code>$
    ...

```
#### Example with parameters table
![workspace level](bdd_example.jpg)


#### With other BDD frameworks 
```
func TestCreateAccount(t *testing.T) {
	given, when, then := GetAccountTest(t)

	given.
		initialized_service()

	when.
		creating_an_account()

	then.
		the_status_code_is_200()
}
```

 
### Cucumber 
https://github.com/r0kas/form3-accountapi-client/blob/master/test/features/account.feature
https://github.com/DATA-DOG/godog 


## Organizing code - Modules

## Examples
https://go.googlesource.com/blog/+/refs/heads/master


# Before Installation
If you already have go installed in old version - remember to remove it.
How to check current version:
```
go version
```
How to remove it:
```
sudo apt-get remove golang
```

# Installation
Remember about (maybe in .profile):
```
export GOROOT=/usr/local/go
# both paths - to golang binaries, to my own golang binaries
export PATH=".......:/usr/local/go/bin:/home/.../go/bin:$PATH"

# Go binaries of my project:
go env -w GOBIN=~/go/bin


```

# Memory management
Go is a language which supports **automatic memory management**, such as automatic memory allocation and automatic garbage collection. 
